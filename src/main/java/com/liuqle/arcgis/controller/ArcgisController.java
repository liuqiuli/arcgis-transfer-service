package com.liuqle.arcgis.controller;

import com.liuqle.arcgis.config.ArcgisProperties;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.Charset;

/**
 * 转发arcgis两种请求
 * 1.http://202.108.145.118:6080/arcgis/rest/services/JCSS/MapServer/2?f=json&callback=dojo.io.script.jsonp_dojoIoScript1._jsonpCallback  直接转发到服务器
 * 2.http://202.108.145.118:6080/arcgis/rest/services/JCSS/MapServer/2/query?f=json&where=1%3D1&returnGeometry=true&spatialRel=esriSpatialRelIntersects&outFields=*&outSR=102100&callback=dojo.io.script.jsonp_dojoIoScript_JCSS_RQGX20._jsonpCallback
 * 封装数据再发送回前端
 *
 * 功能：自定义数据内容，不修改数据格式。
 * author: 刘秋立
 * date：2019-04-17
 */
@RestController()
@RequestMapping(value = "/arcgis/rest/services")
public class ArcgisController {
    @Autowired
    private ArcgisProperties arcgisProperties;
    @Autowired
    private ResourceLoader resourceLoader;

    private static final RestTemplate restTemplate = new RestTemplate();

    @RequestMapping(value = {"/*/MapServer/*", "/*/FeatureServer/*"}, method = RequestMethod.GET)
    public String queryLayerInfo(HttpServletRequest request){
        String queryUrl = arcgisProperties.getUrl() + request.getServletPath() + "?" + request.getQueryString();
        return restTemplate.getForObject(queryUrl, String.class);
    }

    /**
     * 自定义数据放入classpath/data目录下
     * 格式为：{serviceId}-{id}
     * @param request
     * @param serviceId
     * @param id
     * @return
     * @throws Exception
     */
    @RequestMapping(value = {"/{serviceId}/MapServer/{id}/query", "/{serviceId}/FeatureServer/{id}/query"}, method = RequestMethod.GET)
    public String queryLayerData(HttpServletRequest request, @PathVariable(value = "serviceId")String serviceId, @PathVariable(value = "id")String id) throws Exception{
        Resource resource = resourceLoader.getResource("classpath:data/" + serviceId + "-" + id);
        if(resource.exists()){
            return IOUtils.toString(resource.getInputStream(), Charset.forName("UTF-8"));
        } else {
            String queryUrl = arcgisProperties.getUrl() + request.getServletPath() + "?" + request.getQueryString();
            return restTemplate.getForObject(queryUrl, String.class);
        }
    }
}
