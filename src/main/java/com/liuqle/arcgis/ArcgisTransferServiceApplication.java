package com.liuqle.arcgis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableAutoConfiguration
@SpringBootApplication()
public class ArcgisTransferServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ArcgisTransferServiceApplication.class, args);
    }

}
