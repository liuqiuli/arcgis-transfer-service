package com.liuqle.arcgis.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "arcgis")
@Setter
@Getter
public class ArcgisProperties {

    private String url;
}
